from django.urls import path

from .views import (EspecialidadeAPIWiew, EspecialidadesAPIWiew,
                    HardSkillAPIWiew, HardSkillsAPIWiew, TrilhaAPIWiew,
                    TrilhasAPIWiew)

urlpatterns = [
    path('trilhas/', TrilhaAPIWiew.as_view(), name='trilhas'),
    path('trilha/<int:pk>/', TrilhasAPIWiew.as_view(), name='trilha'),
    path('especialidades/', EspecialidadesAPIWiew.as_view(), name='especialidades'),
    path('especialidade/<int:pk>/',
         EspecialidadeAPIWiew.as_view(), name='especialidade'),
    path('hardSkill/<int:pk>', HardSkillAPIWiew.as_view(), name='hardSkill'),
    path('hardSkills/', HardSkillsAPIWiew.as_view(), name='hardSkills'),
]
