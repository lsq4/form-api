from rest_framework import serializers

from .models import Especialidade, HardSkill, Trilha

"""
Serializer de Trilha
"""
class TrilhaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Trilha
        fields = (
            'id',
            'nome', 
            'criacao', 
            'atualizacao', 
            'ativo'
        )
    
    
class EspecialidadeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Especialidade
        fields = (
            'id',
            'nome', 
            'trilhas',
            'criacao', 
            'atualizacao',
            'ativo'
            )
    
    
class HardSkillSerializer(serializers.ModelSerializer):
    class Meta:
        model = HardSkill
        fields = (
            'id',
            'nome',
            'especialidades',
            'trilhas', 
            'criacao',
            'atualizacao',
            'ativo'
            )
