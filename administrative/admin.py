from django.contrib import admin

from .models import Especialidade, HardSkill, Trilha


@admin.register(Trilha)
class TrilhaAdmin(admin.ModelAdmin):
    list_display = ('nome', 'criacao', 'atualizacao', 'ativo')
    
@admin.register(Especialidade)
class EspecialidadeAdmin(admin.ModelAdmin):
    list_display = ('nome', 'trilhas','criacao', 'atualizacao', 'ativo')


@admin.register(HardSkill)
class HardskillAdmin(admin.ModelAdmin):
    list_display = ('nome', 'especialidades', 'trilhas', 'criacao', 'atualizacao', 'ativo')

