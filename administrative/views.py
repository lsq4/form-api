from rest_framework import generics

from .models import Especialidade, HardSkill, Trilha
from .serializers import (EspecialidadeSerializer, HardSkillSerializer,
                          TrilhaSerializer)


class TrilhaAPIWiew(generics.ListCreateAPIView):
    queryset = Trilha.objects.all()
    serializer_class = TrilhaSerializer


class TrilhasAPIWiew(generics.RetrieveUpdateDestroyAPIView):
    queryset = Trilha.objects.all()
    serializer_class = TrilhaSerializer


class EspecialidadesAPIWiew(generics.ListCreateAPIView):
    queryset = Especialidade.objects.all()
    serializer_class = EspecialidadeSerializer


class EspecialidadeAPIWiew(generics.RetrieveUpdateDestroyAPIView):
    queryset = Especialidade.objects.all()
    serializer_class = EspecialidadeSerializer


class HardSkillAPIWiew(generics.RetrieveUpdateDestroyAPIView):
    queryset = HardSkill.objects.all()
    serializer_class = HardSkillSerializer


class HardSkillsAPIWiew(generics.ListCreateAPIView):
    queryset = HardSkill.objects.all()
    serializer_class = HardSkillSerializer
