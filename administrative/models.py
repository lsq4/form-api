from django.db import models

"""
Classe responsavel pelo modelo de Trilha
"""

class Base(models.Model):
    criacao = models.DateTimeField(auto_now_add=True)
    atualizacao = models.DateTimeField(auto_now=True)
    ativo = models.BooleanField(default=True)
    
    class Meta:
        abstract = True


class Trilha(Base):
    nome = models.CharField(max_length=15)

    class Meta:
        verbose_name = 'trilha'
        verbose_name_plural = 'trilhas'

    def __str__(self):
        return self.nome

"""
Classe responsavel pelo modelo de Especialidade
"""

class Especialidade(Base):
    trilhas = models.ForeignKey(Trilha, on_delete=models.CASCADE)
    nome = models.CharField(max_length=25)

    verbose_name = 'nomes'
    verbose_name_plural = 'nomes'

    def __str__(self):
        return self.nome

"""
Classe responsavel pelo modelo de HardSkill
"""

class HardSkill(Base):
    trilhas = models.ForeignKey(Trilha, on_delete=models.CASCADE)
    especialidades = models.ForeignKey(Especialidade, on_delete=models.CASCADE)
    nome = models.CharField(max_length=20)

    def __str__(self):
        return self.nome
